import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Role } from 'src/roles/entities/role.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(Role) private rolesRepository: Repository<Role>,
  ) {}
  create(createUserDto: CreateUserDto) {
    const user = new User();
    user.email = createUserDto.email;
    user.fullName = createUserDto.fullName;
    user.password = createUserDto.password;
    user.gender = createUserDto.gender;
    user.roles = JSON.parse(createUserDto.roles);
    if (createUserDto.image && createUserDto.image !== '') {
      user.image = createUserDto.image;
    }

    return this.usersRepository.save(user);
  }

  findAll() {
    return this.usersRepository.find({ relations: { roles: true } });
  }

  findOne(id: number) {
    return this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
  }
  findOneByEmail(email: string) {
    return this.usersRepository.findOneByOrFail({ email });
  }
  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = new User();
    user.email = updateUserDto.email;
    user.fullName = updateUserDto.fullName;
    user.password = updateUserDto.password;
    user.gender = updateUserDto.gender;
    user.roles = JSON.parse(updateUserDto.roles);
    if (updateUserDto.image && updateUserDto.image !== '') {
      user.image = updateUserDto.image;
    }
    const updateUesr = await this.usersRepository.findOneOrFail({
      where: { id },
      relations: { roles: true },
    });
    updateUesr.email = user.email;
    updateUesr.fullName = user.fullName;
    updateUesr.gender = user.gender;
    updateUesr.password = user.password;
    updateUesr.image = user.image;
    for (const r of user.roles) {
      const searchRole = updateUesr.roles.find((role) => role.id === r.id);
      if (!searchRole) {
        const newRole = await this.rolesRepository.findOneBy({ id: r.id });
        updateUesr.roles.push(newRole);
      }
    }
    for (let i = 0; i < updateUesr.roles.length; i++) {
      const index = user.roles.findIndex(
        (role) => role.id === updateUesr.roles[i].id,
      );
      if (index < 0) {
        updateUesr.roles.splice(i, 1);
      }
    }
    await this.usersRepository.save(updateUesr);
    const result = await this.usersRepository.findOne({
      where: { id },
      relations: { roles: true },
    });
    return result;
  }

  async remove(id: number) {
    const deleteProduct = await this.usersRepository.findOneOrFail({
      where: { id },
    });
    await this.usersRepository.remove(deleteProduct);

    return deleteProduct;
  }
}
